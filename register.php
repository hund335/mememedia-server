<?php
/**
 * Created by PhpStorm.
 * User: frederikhojgaard
 * Date: 08/07/2017
 * Time: 23.20
 */

include include_once('mysql.php');
$mysql = new mysql();
$user = htmlentities($_GET['user']);
$email = htmlentities($_GET['email']);
$pass = htmlentities($_GET['password']);
$confpass = htmlentities($_GET['confirmpassword']);
$bday = htmlentities($_GET['birtday']);
$array = array();


if($mysql->connection()) {
    if ($user == null || $email == null || $pass == null || $confpass == null || $bday == null) {
        $array['status'] = "Error";
        $array['message'] = "All the fields are empty!";
        echo json_encode($array);
        return false;
    }else{

        if ($mysql->checkUsername($user)) {
            $array['status'] = "Error";
            $array['message'] = "This name already exists in the database!";
            echo json_encode($array);
        } else {

            if ($mysql->checkEmail($email)) {
                $array['status'] = "Error";
                $array['message'] = "This email is already registered in the database!";
                echo json_encode($array);
            } else {

                if ($pass != $confpass) {
                    $array['status'] = "Error";
                    $array['message'] = "Your password and confirmation password do not match.";
                    echo json_encode($array);
                } else {

                    if ($bday == "13") {
                        $array['status'] = "Error";
                        $array['message'] = "You have to be over the age 13 to sign up.";
                        echo json_encode($array);
                    } else {

                        $mysql->register($email, $user, $pass, $confpass, $bday);
                        $array['status'] = "Success";
                        $array['message'] = "Your account has been created.";
                        echo json_encode($array);
                    }
                }
            }
        }
    }

} else {
    $array['status'] = "Error";
    $array['message'] = "Couldnt connect to the server!";
    echo json_encode($array);
}


?>