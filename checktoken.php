<?php
/**
 * Created by PhpStorm.
 * User: frederikhojgaard
 * Date: 03/08/2017
 * Time: 19.00
 */

include include_once('mysql.php');
$mysql = new mysql();
$user = htmlentities($_GET['user']);
$token = htmlentities($_GET['token']);
$array = array();


if($mysql->connection()){

    if($user == null || $token == null){
        $array['status'] = "Error";
        $array['message'] = "Nope!";
        print json_encode($array);
    }else {
        if (!$mysql->checkTokenUser($user, $token)) {
            $array['status'] = "Error";
            $array['message'] = "The token is invalid!";
            print json_encode($array);
        } else {
            $array['status'] = "Success";
            $array['message'] = "The token is valid";
            print json_encode($array);
        }
    }

}


?>