<?php
/**
 * Created by PhpStorm.
 * User: frederikhojgaard
 * Date: 05/07/2017
 * Time: 20.33
 */

include include_once('mysql.php');
$mysql = new mysql();
$email = htmlentities($_GET['email']);
$pass = htmlentities($_GET['password']);
//protected passwords / incrypted/decrypted
//$pass = md5(htmlentities($_GET['password']));
$array = array();

if($mysql->connection()){

    //echo $mysql->nub(20);

    if(!$mysql->checkEmail($email)){
        $array['status'] = "Error";
        $array['message'] = "Email and password do not match!";
        echo json_encode($array);

    }else{
        if(!$mysql->checkPass($email, $pass)){
            $array['status'] = "Error";
            $array['message'] = "Email and password do not match!";
            echo json_encode($array);
        }else {
            if ($mysql->checkBanEmail($email)) {
                $array['status'] = "Suspended";
                $array['message'] = $mysql->emailBanReason($email);
                echo json_encode($array);

            }else{
                $array['status'] = "Success";
                $array['message'] = "Logged in.";
                $token = $mysql->createToken();
                $array['user'] = $mysql->getUsername($email);
                $array['session'] = $token;
                $sql = "UPDATE accounts SET Token = '". $token ."' WHERE email = '$email'";
                $mysql->res = mysqli_query($mysql->connect, $sql);
                echo json_encode($array);
            }
        }
    }

}else{
    $array['status'] = "Error";
    $array['message'] = "Couldnt connect to the server!";
    json_encode($array);
}

?>