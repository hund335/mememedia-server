<?php
/**
 * Created by PhpStorm.
 * User: frederikhojgaard
 * Date: 05/07/2017
 * Time: 19.51
 */

class  mysql
{

    var $host = null;
    var $user = null;
    var $pass = null;
    var $db = null;
    var $connect = null;
    var $res = null;
    var $res2 = null;

    function __construct()
    {

        //$mysql = parse_ini_file('Mysql.ini');
        $mysql = parse_ini_file('/var/MySql.ini');
        $this->host = $mysql['host'];
        $this->user = $mysql['user'];
        $this->pass = $mysql['pass'];
        $this->db = $mysql['db'];

    }


    public function connection()
    {
        $this->connect = new mysqli($this->host, $this->user, $this->pass, $this->db);
        if (mysqli_connect_error()) {
            return false;
        }
        mysql_set_charset($this->connect, 'utf8');
        return true;
    }


    public function closeConnect()
    {
        if ($this->connect != null) {
            mysql_close($this->connect);
        }

    }


    public function checkEmail($email)
    {
        $sql = "SELECT email FROM accounts WHERE email = '$email'";
        $this->res = mysqli_query($this->connect, $sql);
        if (mysqli_num_rows($this->res) <= 0) {
            return false;
        }
        return true;
    }

    public function getUsername($email){
        $sql = "SELECT email, username FROM accounts where email = '$email'";
        $this->res = mysqli_query($this->connect, $sql);
        $row = mysqli_fetch_array($this->res);
        return $row['username'];

    }

    public function checkUsername($user)
    {
        $sql = "SELECT username FROM accounts WHERE username = '$user'";
        $this->res = mysqli_query($this->connect, $sql);
        if (mysqli_num_rows($this->res) <= 0) {
            return false;
        }
        return true;
    }

    public function checkPass($email, $password)
    {
        $sql = "SELECT email, password FROM accounts WHERE email = '$email'";
        $this->res = mysqli_query($this->connect, $sql);
        $row = mysqli_fetch_array($this->res);
        if ($row['password'] != $password) {
            return false;
        }
        return true;
    }

    public function checkTokenUser($user, $token)
    {
        if($user == null || $token == null) {
            return false;
        }else {
            $sql = "SELECT username, token FROM accounts WHERE username = '$user'";
            $this->res = mysqli_query($this->connect, $sql);
            $row = mysqli_fetch_array($this->res);
            if ($row['token'] != $token) {
                return false;
            }
        }
        return true;
    }

    public function register($email, $user, $pass, $confpass, $bday)
    {
        if ($email == null || $user == null || $pass == null || $confpass == null || $bday == null) {
            echo "Nothing to see";
            return false;
        }
        $sql = "INSERT INTO accounts (Email, Username, Password, Birthday)
      VALUES ('$email', '$user', '$pass', '$bday')";
        $this->res = mysqli_query($this->connect, $sql);
    }


    public function comments($user, $token, $id){

        if($user == null || $token == null || $id == null){
            $array[] = array("status" => "Error", "message" => "Couldnt load the content!");
            print(json_encode($array));

        }else{
            if(!$this->checkTokenUser($user, $token)){
                $array[] = array("status" => "Error", "message" => "Couldnt load the content!");
                print(json_encode($array));

            }else{
                if($this->checkBanUser($user)){
                    $array[] = array("status" => "Suspended", "message" => $this->userBanReason($user));
                    print(json_encode($array));

                }else{
                    $sql = "SELECT meme, poster, comment, date FROM comments WHERE meme = '$id'";
                    $this->res = mysqli_query($this->connect, $sql);
                    if(mysqli_num_rows($this->res) > 0) {
                        while ($rows = mysqli_fetch_array($this->res)) {
                            $array[] = $rows;
                        }
                    }else{
                        $array[] = array("status" => "Error", "message" => "Couldnt find any comments, for this meme!");
                    }
                    print(json_encode($array));
                }
            }

        }

    }

    public function getMeme($id, $user, $token)
    {

        if ($id == null || $user == null || $token == null) {
            $array = array();
            $array['status'] = "Error";
            $array['message'] = "Couldnt load the content!";
            print json_encode($array);

        } else {
            if (!$this->checkTokenUser($user, $token)) {

                $array['status'] = "Error";
                $array['message'] = "Couldnt load the content!";
                print json_encode($array);

            } else {
                if ($this->checkBanUser($user)) {
                    $array['status'] = "Suspended";
                    $array['message'] = $this->userBanReason($user);

                } else {

                    $sql = "SELECT id, poster, title, photo, date FROM memes WHERE id = '$id'";
                    $this->res = mysqli_query($this->connect, $sql);
                    $rows = mysqli_fetch_array($this->res);
                    $array['id'] = $rows['id'];
                    $array['poster'] = $rows['poster'];
                    $array['title'] = $rows['title'];
                    $array['photo'] = $rows['photo'];
                    $array['date'] = $rows['date'];
                    print json_encode($array);

                }
            }
        }
    }


    public function getMemes($user, $token)
    {

        if ($user == null || $token == null) {

            $array = array();
            $array[] = array("status" => "Error", "message" => "Couldnt load the content!");
            print json_encode($array);

        } else {
            if (!$this->checkTokenUser($user, $token)) {

                $array[] = array("status" => "Error", "message" => "Couldnt load the content!");
                print json_encode($array);

            } else {
                if ($this->checkBanUser($user)) {
                    $array[] = array("status" => "Suspended", "message" => $this->userBanReason($user));
                    print json_encode($array);

                } else {

                    $sql = "SELECT id, poster, title, photo, date FROM memes";
                    $this->res = mysqli_query($this->connect, $sql);;
                    while ($rows = mysqli_fetch_array($this->res)) {
                        $array[] = $rows;
                    }
                    print json_encode($array);
                }
            }
        }
    }


    public function getMemesByUser($target, $user, $token)
    {
        if ($target == null ||$user == null || $token == null) {
            $array = array();
            $array[] = array("status" => "Error", "message" => "Couldnt load the content!");
            print json_encode($array);

        } else {
            if (!$this->checkTokenUser($user, $token)) {

                $array[] = array("status" => "Error", "message" => "Couldnt load the content!");
                print json_encode($array);

            } else {
                if ($this->checkBanUser($user)) {
                    $array[] = array("status" => "Suspended", "message" => $this->userBanReason($user));
                    print json_encode($array);

                } else {
                    $sql = "SELECT id, poster, title, photo, date FROM memes WHERE poster = '$target'";
                    $this->res = mysqli_query($this->connect, $sql);
                    while ($rows = mysqli_fetch_array($this->res)) {
                        $array[] = $rows;
                    }

                    print json_encode($array);

                }

            }
        }
    }

    public function checkBanEmail($email)
    {
        $sql = "SELECT id, email FROM accounts WHERE email = '$email'";
        $this->res = mysqli_query($this->connect, $sql);
        $row = mysqli_fetch_array($this->res);
        $id = $row['id'];

        $check = "SELECT id, reason FROM suspended WHERE id = '$id'";
        $this->res2 = mysqli_query($this->connect, $check);
        $row2 = mysqli_fetch_array($this->res2);
        if (mysqli_num_rows($this->res2) <= 0) {
            return false;
        }
        return true;

    }

    public function userBanReason($user)
    {
        $sql = "SELECT id, username FROM accounts WHERE username = '$user'";
        $this->res = mysqli_query($this->connect, $sql);
        $row = mysqli_fetch_array($this->res);
        $id = $row['id'];

        $check = "SELECT id, reason FROM suspended WHERE id = '$id'";
        $this->res2 = mysqli_query($this->connect, $check);
        $row2 = mysqli_fetch_array($this->res2);
        if (mysqli_num_rows($this->res2) <= 0) {
            return false;
        }
        return $row2['reason'];

    }

    public function emailBanReason($email)
    {
        $sql = "SELECT id, email FROM accounts WHERE email = '$email'";
        $this->res = mysqli_query($this->connect, $sql);
        $row = mysqli_fetch_array($this->res);
        $id = $row['id'];

        $check = "SELECT id, reason FROM suspended WHERE id = '$id'";
        $this->res2 = mysqli_query($this->connect, $check);
        $row2 = mysqli_fetch_array($this->res2);
        if (mysqli_num_rows($this->res2) <= 0) {
            return false;
        }
        return $row2['reason'];

    }

    public function checkBanUser($user)
    {
        $sql = "SELECT id, username FROM accounts WHERE username = '$user'";
        $this->res = mysqli_query($this->connect, $sql);
        $row = mysqli_fetch_array($this->res);
        $id = $row['id'];

        $check = "SELECT id, reason FROM suspended WHERE id = '$id'";
        $this->res2 = mysqli_query($this->connect, $check);
        $row2 = mysqli_fetch_array($this->res2);
        if (mysqli_num_rows($this->res2) <= 0) {
            return false;
        }
        return true;

    }


        public function createToken()
        {

            $chars = "123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
            $token = "";
            $max = strlen($chars);

            for ($i = 0; $i < 31; $i++) {
                $token .= $chars[mt_rand(0, $max)];
            }

            return $token;
        }
}